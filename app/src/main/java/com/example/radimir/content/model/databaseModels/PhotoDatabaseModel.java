package com.example.radimir.content.model.databaseModels;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.radimir.content.model.database.DBContract;
import com.google.gson.annotations.SerializedName;

/**
 * Created by radimir on 18.12.2015.
 */
public class PhotoDatabaseModel extends DatabaseModel {
    @SerializedName("albumId")
    private int albumId;
    @SerializedName("title")
    private String title;
    @SerializedName("url")
    private String photoUrl;
    @SerializedName("thumbnailUrl")
    private String thumbnailUrl;

    public PhotoDatabaseModel() {
    }

    public PhotoDatabaseModel(Cursor cursor) {
        super(cursor);
        this.title = cursor.getString(cursor.getColumnIndex(DBContract.PhotoTable.COLLUMN_TITLE));
        this.photoUrl = cursor.getString(cursor.getColumnIndex(DBContract.PhotoTable.COLLUMN_PHOTO_URL));
        this.thumbnailUrl = cursor.getString(cursor.getColumnIndex(DBContract.PhotoTable.COLLUMN_THUMBNAIL));
        this.albumId = cursor.getInt(cursor.getColumnIndex(DBContract.PhotoTable.COLLUMN_ALBUM_ID));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    @Override
    public ContentValues toContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBContract.COLLUMN_REMOTE_ID, remoteId);
        contentValues.put(DBContract.PhotoTable.COLLUMN_THUMBNAIL, thumbnailUrl);
        contentValues.put(DBContract.PhotoTable.COLLUMN_ALBUM_ID, albumId);
        contentValues.put(DBContract.PhotoTable.COLLUMN_TITLE, title);
        contentValues.put(DBContract.PhotoTable.COLLUMN_PHOTO_URL, photoUrl);
        return contentValues;
    }

}
