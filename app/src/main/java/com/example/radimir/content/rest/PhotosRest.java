package com.example.radimir.content.rest;

import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by radimir on 17.12.2015.
 */
public interface PhotosRest {
    @GET("/photos")
    Call<List<PhotoDatabaseModel>> getPhotos();

    @GET("/photos")
    Call<List<PhotoDatabaseModel>> getPhotosByAlbumId(@Query("albumId") String albumId);
}
