package com.example.radimir.content.rest.restServices;

/**
 * Created by radimir on 22.12.2015.
 */
public interface PhotoRestServiceListener {

    void OnSuccessLoadAllPhotos();

    void OnSuccessLoadByAlbumId(int albumId);

    void OnSuccessLoadByTitle(String title);

    void OnError(int ErrorCode);
}
