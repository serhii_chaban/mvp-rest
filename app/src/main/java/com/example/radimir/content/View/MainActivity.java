package com.example.radimir.content.View;

import android.graphics.Point;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;
import com.example.radimir.content.R;
import com.example.radimir.content.View.adapter.PhotoAdapter;
import com.example.radimir.content.View.presenter.MainActivityPresenter;
import com.example.radimir.content.View.presenter.interfaces.DataLoadedCallback;

import java.util.List;

public class MainActivity extends AppCompatActivity implements DataLoadedCallback {
    private MainActivityPresenter activityPresenter;
    private PhotoAdapter photoAdapter;
    private RecyclerView photosGrid;
    private FloatingActionButton loadNewPhotosButton;
    private RadioGroup radioGroup;
    private EditText queryEditText;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activityPresenter = new MainActivityPresenter(this, this);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        // int height = size.y;
        photoAdapter = new PhotoAdapter(this, width / 2);
        initViews();
        activityPresenter.loadAllPhotosToViewFromDatabase();
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.main_progress_bar);
        queryEditText = (EditText) findViewById(R.id.edit_query);
        radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

            }
        });
        photosGrid = (RecyclerView) findViewById(R.id.photos_grid);
        int spanCount = 2;
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, spanCount);
        photosGrid.setAdapter(photoAdapter);
        photosGrid.setLayoutManager(gridLayoutManager);
        loadNewPhotosButton = (FloatingActionButton) findViewById(R.id.load_button);
        loadNewPhotosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query = queryEditText.getText().toString();
                if (!query.isEmpty()) {
                    int checkedId = radioGroup.getCheckedRadioButtonId();
                    switch (checkedId) {
                        case R.id.byAlbumId:
                            activityPresenter.loadPhotosByAlbumId(query);
                            break;
                        case R.id.byId:
                            break;
                        case R.id.byTitle:
                            break;
                        default:
                    }

                } else {
                    activityPresenter.loadAllPhotos();
                }
                progressBar.setVisibility(View.VISIBLE);
                v.setEnabled(false);
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityPresenter.unregister(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityPresenter.register(null);
    }

    @Override
    public void newPhotosLoaded(List<PhotoDatabaseModel> photoDatabaseModels) {
        photoAdapter.setLoadedPhotos(photoDatabaseModels);
        progressBar.setVisibility(View.GONE);
        loadNewPhotosButton.setEnabled(true);
    }
}
