package com.example.radimir.content.helperClasses.managers;

import android.content.Context;
import android.util.SparseArray;

import java.util.HashMap;

/**
 * Created by radimir on 22.12.2015.
 */
public class ServiceCallbacksManager {
    private static SparseArray<Object> callbacks = new SparseArray<>();
    public static ServiceCallbacksManager instance = null;

    private ServiceCallbacksManager() {
    }

    public static ServiceCallbacksManager getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new ServiceCallbacksManager();
        }
        return instance;
    }

    public static ServiceCallbacksManager getInstance() {
        return instance;
    }

    public static int register(Object object) {
        int key = object.hashCode();
        callbacks.put(key, object);
        return key;
    }

    public static void unregisterObject(Object object) {
        int key = object.hashCode();
        callbacks.remove(key);
    }

    public static Object getCallback(int hashKey) {
        return callbacks.get(hashKey, null);
    }

    public void Terminate() {
        callbacks.clear();
    }
}
