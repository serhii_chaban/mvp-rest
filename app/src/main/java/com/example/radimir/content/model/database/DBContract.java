package com.example.radimir.content.model.database;

/**
 * Created by radimir on 17.12.2015.
 */
public class DBContract {
    private static final String TAG = "DBContract";
    private static final String COMMA_SEP = ", ";
    public static final String ID = "id";
    public static final String EMPTY = " ";
    public static final String COLLUMN_REMOTE_ID = "remoteId";
    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS";

    public static class PhotoTable {
        public static final String TABLE_NAME = "photo";
        public static final String COLLUMN_TITLE = "title";
        public static final String COLLUMN_ALBUM_ID = "albumId";
        public static final String COLLUMN_PHOTO_URL = "url";
        public static final String COLLUMN_THUMBNAIL = "thumbnailUrl";
        public static final String createPhotoTableQuery = CREATE_TABLE
                + EMPTY
                + " \'" + TABLE_NAME + "\' "
                + EMPTY
                + "( "
                + COLLUMN_REMOTE_ID + EMPTY + "INTEGER PRIMARY KEY" + COMMA_SEP
                + COLLUMN_ALBUM_ID + EMPTY + "INTEGER" + COMMA_SEP
                + COLLUMN_TITLE + EMPTY + "TEXT" + COMMA_SEP
                + COLLUMN_PHOTO_URL + EMPTY + "TEXT" + COMMA_SEP
                + COLLUMN_THUMBNAIL + EMPTY + "TEXT"
                + " )";
    }
}
