package com.example.radimir.content.View.presenter.interfaces;

import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;

import java.util.List;

/**
 * Created by radimir on 20.12.2015.
 */
public interface DataLoadedCallback {
    void newPhotosLoaded(List<PhotoDatabaseModel> photoDatabaseModels);
}
