package com.example.radimir.content.View.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;
import com.example.radimir.content.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by radimir on 20.12.2015.
 */
public class PhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "PhotoAdapter";
    private List<PhotoDatabaseModel> listOfLoadedPhotos = new ArrayList<>();
    private Context context;
    final int imageWidth;

    public PhotoAdapter(Context context, int imageWidth) {
        this.context = context;
        this.imageWidth = imageWidth;
    }

    private class PhotoViewHolder extends RecyclerView.ViewHolder {
        ImageView photoImageHolder;
        ProgressBar photoIsLoadingProgressBar;
        TextView titleView;

        public PhotoViewHolder(View itemView) {
            super(itemView);
            photoImageHolder = (ImageView) itemView.findViewById(R.id.image_view);
            titleView = (TextView) itemView.findViewById(R.id.text_view);
            photoIsLoadingProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.photo_view_item_layout, parent, false);
        PhotoViewHolder viewHolder = new PhotoViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        PhotoDatabaseModel photoModel = getItem(position);
        Picasso.with(context).load(photoModel.getThumbnailUrl()).resize(imageWidth, imageWidth).centerCrop().into(((PhotoViewHolder) holder).photoImageHolder, new Callback() {
            @Override
            public void onSuccess() {
                ((PhotoViewHolder) holder).photoIsLoadingProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
        ((PhotoViewHolder) holder).titleView.setText(photoModel.getTitle());
    }

    public PhotoDatabaseModel getItem(int position) {
        return listOfLoadedPhotos.get(position);
    }

    @Override
    public int getItemCount() {
        return listOfLoadedPhotos.size();
    }

    public void addNewLoadedPhotos(List<PhotoDatabaseModel> listOfLoadedPhotos) {
        this.listOfLoadedPhotos.addAll(listOfLoadedPhotos);
        notifyDataSetChanged();
    }

    public void addNewLoadedPhotos(int position, List<PhotoDatabaseModel> listOfLoadedPhotos) {
        this.listOfLoadedPhotos.addAll(position, listOfLoadedPhotos);
        notifyDataSetChanged();
    }

    public void setLoadedPhotos(List<PhotoDatabaseModel> listOfLoadedPhotos) {
        this.listOfLoadedPhotos.clear();
        this.listOfLoadedPhotos.addAll(listOfLoadedPhotos);
        notifyDataSetChanged();
    }

    public void clearLoadedPhotos(List<PhotoDatabaseModel> listOfLoadedPhotos) {
        this.listOfLoadedPhotos.clear();
        notifyDataSetChanged();
    }
}
