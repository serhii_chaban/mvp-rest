package com.example.radimir.content.model.databaseModels;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.radimir.content.model.database.DBContract;
import com.google.gson.annotations.SerializedName;

/**
 * Created by radimir on 18.12.2015.
 */

public abstract class DatabaseModel {
    @SerializedName("id")
    int remoteId = -1;

    public DatabaseModel() {
    }

    public DatabaseModel(Cursor cursor) {
        remoteId = cursor.getInt(cursor.getColumnIndex(DBContract.COLLUMN_REMOTE_ID));
    }

    public int getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(int remoteId) {
        this.remoteId = remoteId;
    }

    public abstract ContentValues toContentValues();

}
