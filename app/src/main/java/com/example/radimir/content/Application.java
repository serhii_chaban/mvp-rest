package com.example.radimir.content;

import com.example.radimir.content.helperClasses.managers.ServiceCallbacksManager;
import com.example.radimir.content.model.database.DatabaseManager;
import com.facebook.stetho.Stetho;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created by radimir on 17.12.2015.
 */
public class Application extends android.app.Application {
    private static final String TAG = "Application";
    DatabaseManager databaseManager;
    ServiceCallbacksManager serviceCallbacksManager;
    public Application() {
        super();
    }

    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();
        databaseManager = DatabaseManager.getInstance(this);
        Stetho.initializeWithDefaults(this);
        OkHttpClient okHttpClient = new OkHttpClient();
        String baseUrl = "http://jsonplaceholder.typicode.com";
        Gson gson = new GsonBuilder().create();
        retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public static Retrofit getRetrofit() {
        return retrofit;
    }

    @Override
    public void onTerminate() {
        databaseManager.terminate();
        databaseManager = null;
        super.onTerminate();

    }

}
