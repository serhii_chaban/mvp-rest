package com.example.radimir.content.View.presenter;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.example.radimir.content.Application;
import com.example.radimir.content.helperClasses.calbacks.ListenerSubscribeCallback;
import com.example.radimir.content.helperClasses.managers.ServiceCallbacksManager;
import com.example.radimir.content.model.database.DBContract;
import com.example.radimir.content.model.database.DatabaseManager;
import com.example.radimir.content.model.databaseModels.DatabaseModel;
import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;
import com.example.radimir.content.View.presenter.interfaces.DataLoadedCallback;
import com.example.radimir.content.rest.PhotosRest;
import com.example.radimir.content.rest.restServices.PhotoRestService;
import com.example.radimir.content.rest.restServices.PhotoRestServiceListener;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by radimir on 17.12.2015.
 */
public class MainActivityPresenter implements ListenerSubscribeCallback, PhotoRestServiceListener {
    private static final String TAG = "MainActivityPresenter";
    private Context activityContext;
    private DatabaseManager databaseManager;
    private DataLoadedCallback dataLoadedCallback;
    private static int myCallbackId = -1;

    /**
     * Instantiates a new Main activity presenter.
     *
     * @param activityContext    the activity context
     * @param dataLoadedCallback the data loaded callback
     */
    public MainActivityPresenter(Context activityContext, DataLoadedCallback dataLoadedCallback) {
        this.activityContext = activityContext;
        databaseManager = databaseManager.getInstance();
        this.dataLoadedCallback = dataLoadedCallback;

    }

    /**
     * Load all photos from REST.
     */
    public void loadAllPhotos() {
        PhotoRestService.getAllPhotos(activityContext, myCallbackId);
    }

    /**
     * Load photos by album id from REST.
     *
     * @param albumId the album id
     */
    public void loadPhotosByAlbumId(String albumId) {
        PhotoRestService.getPhotosByAlbumId(activityContext, Integer.parseInt(albumId), myCallbackId);
    }

    /**
     * Load all photos to view from database.
     */
    public void loadAllPhotosToViewFromDatabase() {
        Log.d(TAG, "loadPhotosToView()");
        Cursor queryResult = databaseManager.getAllObjects(DBContract.PhotoTable.TABLE_NAME);
        ArrayList<PhotoDatabaseModel> photoDatabaseModels = new ArrayList<>();
        int queryCount = queryResult.getCount();
        if (queryCount != -1 && queryCount != 0) {
            queryResult.moveToFirst();
            while (!queryResult.isLast()) {
                photoDatabaseModels.add(new PhotoDatabaseModel(queryResult));
                queryResult.moveToNext();
            }
            queryResult.close();
            dataLoadedCallback.newPhotosLoaded(photoDatabaseModels);
        }

    }

    /**
     * Load photos by album id from database.
     *
     * @param albumId the album id
     */
    public void loadPhotosByAlbumIdFromDatabase(String albumId) {
        String[] values = {albumId};
        loadPhotosToViewFromDatabaseByParam(DBContract.PhotoTable.COLLUMN_ALBUM_ID, values);
    }

    private void loadPhotosToViewFromDatabaseByParam(String paramName, String[] paramValue) {
        Log.d(TAG, "loadPhotosToViewFromDatabaseByParam() called with: " + "paramName = [" + paramName + "], paramValue = [" + paramValue + "]");
        ;
        String selection = paramName + "=?";
        Cursor queryResult = databaseManager.getByParam(DBContract.PhotoTable.TABLE_NAME, selection, paramValue);
        //Cursor queryResult = databaseManager.getAllObjects(DBContract.PhotoTable.TABLE_NAME);

        ArrayList<PhotoDatabaseModel> photoDatabaseModels = new ArrayList<>();
        int queryCount = queryResult.getCount();
        if (queryCount != -1 && queryCount != 0) {
            queryResult.moveToFirst();
            while (!queryResult.isLast()) {
                photoDatabaseModels.add(new PhotoDatabaseModel(queryResult));
                queryResult.moveToNext();
            }
            dataLoadedCallback.newPhotosLoaded(photoDatabaseModels);
            queryResult.close();
        }
    }

    @Override
    public int register(Object obj) {
        myCallbackId = ServiceCallbacksManager.register(this);
        return myCallbackId;
    }

    @Override
    public boolean unregister(Object object) {
        ServiceCallbacksManager.unregisterObject(this);
        myCallbackId = -1;
        return false;
    }

    @Override
    public void OnSuccessLoadAllPhotos() {
        loadAllPhotosToViewFromDatabase();
    }

    @Override
    public void OnSuccessLoadByAlbumId(int albumId) {
        loadPhotosByAlbumIdFromDatabase(String.valueOf(albumId));
    }

    @Override
    public void OnSuccessLoadByTitle(String title) {

    }

    @Override
    public void OnError(int ErrorCode) {

    }
}
