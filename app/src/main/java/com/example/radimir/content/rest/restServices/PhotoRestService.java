package com.example.radimir.content.rest.restServices;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;

import com.example.radimir.content.Application;
import com.example.radimir.content.helperClasses.managers.ServiceCallbacksManager;
import com.example.radimir.content.model.database.DBContract;
import com.example.radimir.content.model.database.DatabaseManager;
import com.example.radimir.content.model.databaseModels.DatabaseModel;
import com.example.radimir.content.model.databaseModels.PhotoDatabaseModel;
import com.example.radimir.content.rest.PhotosRest;

import java.io.IOException;
import java.util.List;
import java.util.Objects;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by radimir on 22.12.2015.
 */
public class PhotoRestService extends IntentService {
    private DatabaseManager databaseManager;
    private PhotosRest photosRest;
    private static final String ACTION_GET = "ACTION_GET";
    private static final String KEY_ALBUM_ID = "albumId";
    private static final String KEY_ALL = "all";
    private static final String KEY_CALLBACK_ID = "CALLBACK_ID";
    private static final String KEY_TITLE = "title";
    private static final String query = "query";
    private static final String TAG = "PhotoRestService";
    private static final String value = "value";
    private Handler mainHandler;

    public PhotoRestService() {
        super(TAG);
    }

    public static void getAllPhotos(Context context, int callbackId) {
        Intent getAllPhotosIntent = new Intent(context, PhotoRestService.class);
        getAllPhotosIntent.setAction(ACTION_GET);
        getAllPhotosIntent.putExtra(query, KEY_ALL);
        getAllPhotosIntent.putExtra(KEY_CALLBACK_ID, callbackId);
        context.startService(getAllPhotosIntent);
    }

    public static void getPhotosByAlbumId(Context context, int albumId, int callbackId) {
        Intent getPhotosIntent = new Intent(context, PhotoRestService.class);
        getPhotosIntent.setAction(ACTION_GET);
        getPhotosIntent.putExtra(query, KEY_ALBUM_ID);
        getPhotosIntent.putExtra(value, albumId);
        getPhotosIntent.putExtra(KEY_CALLBACK_ID, callbackId);
        context.startService(getPhotosIntent);
    }

    public static void getPhotosByTitle(Context context, String title, int callbackId) {
        Intent getPhotosIntent = new Intent(context, PhotoRestService.class);
        getPhotosIntent.setAction(ACTION_GET);
        getPhotosIntent.putExtra(query, KEY_TITLE);
        getPhotosIntent.putExtra(value, title);
        getPhotosIntent.putExtra(KEY_CALLBACK_ID, callbackId);
        context.startService(getPhotosIntent);
    }


    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public PhotoRestService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mainHandler = new Handler(getApplicationContext().getMainLooper());
        photosRest = Application.getRetrofit().create(PhotosRest.class);
        databaseManager = databaseManager.getInstance();
        if (!intent.getAction().isEmpty() && intent.getAction().equals(ACTION_GET)) {
            int callbackId = intent.getIntExtra(KEY_CALLBACK_ID, -1);
            switch (intent.getStringExtra(query)) {
                case KEY_ALBUM_ID:
                    int albumIdValue = intent.getIntExtra(value, 0);
                    loadPhotosByAlbumId(albumIdValue, callbackId);
                    break;
                case KEY_TITLE:
                    String titleValue = intent.getStringExtra(value);
                    break;
                default:
                    loadAllPhotos(callbackId);
            }

        }
    }

    /**
     * Load new photos.
     */
    public void loadAllPhotos(final int callbackId) {
        Call<List<PhotoDatabaseModel>> call = photosRest.getPhotos();
        Log.d(TAG, "loadAllPhotos() called with: " + "");
        try {
            Response<List<PhotoDatabaseModel>> response = call.execute();
            if (response.isSuccess()) {
                List<PhotoDatabaseModel> responseBody = response.body();
                final Object photosIsLoadedCallback = ServiceCallbacksManager.getCallback(callbackId);
                DatabaseModel[] databaseModels = new DatabaseModel[responseBody.size()];
                databaseModels = responseBody.toArray(databaseModels);
                databaseManager.saveObjects(DBContract.PhotoTable.TABLE_NAME, databaseModels);
                if (photosIsLoadedCallback != null)
                    if (photosIsLoadedCallback instanceof PhotoRestServiceListener) {
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                ((PhotoRestServiceListener) photosIsLoadedCallback).OnSuccessLoadAllPhotos();
                            } // This is your code
                        };
                        mainHandler.post(myRunnable);

                    }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void loadPhotosByAlbumId(final int albumId, int callbackId) {
        Log.d(TAG, "loadPhotosByAlbumId() called with: " + "albumId = [" + albumId + "]");
        Call<List<PhotoDatabaseModel>> call = photosRest.getPhotosByAlbumId(String.valueOf(albumId));
        try {
            Response<List<PhotoDatabaseModel>> response = call.execute();
            if (response.isSuccess()) {
                List<PhotoDatabaseModel> responseBody = response.body();
                final Object photosIsLoadedCallback = ServiceCallbacksManager.getCallback(callbackId);
                databaseManager.saveObjects(DBContract.PhotoTable.TABLE_NAME, responseBody.toArray(new DatabaseModel[responseBody.size()]));
                if (photosIsLoadedCallback != null)
                    if (photosIsLoadedCallback instanceof PhotoRestServiceListener) {
                        Runnable myRunnable = new Runnable() {
                            @Override
                            public void run() {
                                ((PhotoRestServiceListener) photosIsLoadedCallback).OnSuccessLoadByAlbumId(albumId);
                            } // This is your code
                        };
                        mainHandler.post(myRunnable);
                    }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
