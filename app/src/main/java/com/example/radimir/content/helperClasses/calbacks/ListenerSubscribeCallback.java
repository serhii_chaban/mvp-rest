package com.example.radimir.content.helperClasses.calbacks;

import java.util.Objects;

/**
 * Created by radimir on 22.12.2015.
 */
public interface ListenerSubscribeCallback {
    int register(Object obj);

    boolean unregister(Object object);
}
