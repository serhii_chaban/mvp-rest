package com.example.radimir.content.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.radimir.content.model.databaseModels.DatabaseModel;

/**
 * Created by radimir on 17.12.2015.
 */
public class DatabaseManager {
    /**
     * The Db open helper.
     */
    DBOpenHelper dbOpenHelper;
    /**
     * The Database.
     */
    SQLiteDatabase database;
    private static final String TAG = "DatabaseManager";
    private static DatabaseManager instance = null;
    private Context context;

    private DatabaseManager(Context context) {
        this.context = context;
        dbOpenHelper = new DBOpenHelper(context);
        database = dbOpenHelper.getWritableDatabase();
    }


    /**
     * Save objects.
     *
     * @param table         the table name
     * @param databaseModel the object which extends database model with overriden method toContentValues
     */
    public void saveObject(String table, DatabaseModel databaseModel) {
        ContentValues contentValues = databaseModel.toContentValues();
        Long id = contentValues.getAsLong(DBContract.COLLUMN_REMOTE_ID);
        database.insertWithOnConflict(table, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public Cursor getAllObjects(String table) {
        return database.query(table, null, null, null, null, null, null, null);
    }

    public Cursor getByParam(String table, String selection, String[] arguments) {
        return database.query(table, null, selection, arguments, null, null, null);
    }
    public Cursor getByParams(String table, String selection, String[] arguments) {
        return database.query(table, null, selection, arguments, null, null, null);
    }
    public void saveObjects(String table, DatabaseModel[] databaseModel) {
        int arrayLength = databaseModel.length;
        database.beginTransaction();
        for (int i = 0; i < arrayLength; i++) {
            saveObject(table, databaseModel[i]);
        }
        database.setTransactionSuccessful();
        database.endTransaction();
    }

    /**
     * Gets instance of singletone.
     *
     * @param applicationContext the application context
     * @return the instance
     */
    public static DatabaseManager getInstance(Context applicationContext) {
        if (instance == null) {
            instance = new DatabaseManager(applicationContext);
        }
        return instance;
    }

    public static DatabaseManager getInstance() {
        return instance;
    }

    /**
     * terminate. when applications is turns off close database
     */
    public void terminate() {
        dbOpenHelper.close();
        instance = null;
    }
}
